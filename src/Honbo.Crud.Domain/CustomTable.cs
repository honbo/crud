﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace Honbo.Crud
{
    public class CustomTable : Entity<int>
    {
        public string Name { get; set; }
        public List<Table> Tables { get; set; }
    }
}
