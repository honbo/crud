﻿using Volo.Abp.Settings;

namespace Honbo.Crud.Settings
{
    public class CrudSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(CrudSettings.MySetting1));
        }
    }
}
