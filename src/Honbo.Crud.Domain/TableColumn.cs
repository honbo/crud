﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace Honbo.Crud
{
    public class TableColumn : Entity<int>
    {
        public string Name { get; set; }

        public Table Table { get; set; }
    }
}
