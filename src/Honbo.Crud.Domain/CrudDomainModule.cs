﻿using Microsoft.Extensions.DependencyInjection;
using Honbo.Crud.Settings;
using Volo.Abp.Auditing;
using Volo.Abp.Domain;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.Settings;

namespace Honbo.Crud
{
    [DependsOn(
        typeof(AbpDddDomainModule),
        typeof(AbpAuditingModule))] //TODO: Replace with AbpAuditLoggingDomainModule when it's done.
    public class CrudDomainModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.Configure<SettingOptions>(options =>
            {
                options.DefinitionProviders.Add<CrudSettingDefinitionProvider>();
            });

            context.Services.AddAssemblyOf<CrudDomainModule>();
        }
    }
}
