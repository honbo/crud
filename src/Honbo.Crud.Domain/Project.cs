﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace Honbo.Crud
{
    public class Project : Entity<int>
    {
        public string Name { get; set; }

        #region Db
        public string DbType { get; set; }
        public string DbConnectionString { get; set; }
        #endregion
    }
}
