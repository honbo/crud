﻿using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace Honbo.Crud.Permissions
{
    public class CrudPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(CrudPermissions.GroupName);

            //Define your own permissions here. Examaple:
            //myGroup.AddPermission(CrudPermissions.MyPermission1, L("Permission:MyPermission1"));
        }

    }
}
