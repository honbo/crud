﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace Honbo.Crud
{
    public class Table : Entity<int>
    {
        public string Name { get; set; }


        public List<TableColumn> Columns { get; set; }

        public Project Project { get; set; }
    }
}
