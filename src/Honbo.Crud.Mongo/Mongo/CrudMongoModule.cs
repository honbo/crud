﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;

namespace Honbo.Crud.Mongo
{
    [DependsOn(
        typeof(CrudDomainModule),
        typeof(AbpMongoDbModule))]
    public class CrudMongoModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddMongoDbContext<CrudDbContext>(options =>
            {
                //Remove "includeAllEntities: true" to create default repositories only for aggregate roots
                options.AddDefaultRepositories(true);
            });

            context.Services.AddAssemblyOf<CrudMongoModule>();
        }
    }
}