﻿using Microsoft.Extensions.DependencyInjection;
using Honbo.Crud.Permissions;
using Volo.Abp.Application;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace Honbo.Crud
{
    [DependsOn(
        typeof(CrudDomainModule),
        typeof(AbpDddApplicationModule))]
    public class CrudApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.Configure<PermissionOptions>(options =>
            {
                options.DefinitionProviders.Add<CrudPermissionDefinitionProvider>();
            });

            context.Services.Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddProfile<CrudApplicationAutoMapperProfile>();
            });

            context.Services.AddAssemblyOf<CrudApplicationModule>();
        }
    }
}
