﻿using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Honbo.Crud
{
    public class Program
    {
        public static int Main(string[] args)
        {
            BuildWebHostInternal(args).Run();
            return 0;
        }

        public static IWebHost BuildWebHostInternal(string[] args)
        {
            return new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();
        }
    }
}